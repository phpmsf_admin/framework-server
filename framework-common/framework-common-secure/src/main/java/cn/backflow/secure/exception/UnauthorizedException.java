package cn.backflow.secure.exception;

/**
 * 未验证异常
 * Created by hunan on 2017/5/21.
 */
public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {}

    public UnauthorizedException(String msg) {
        super(msg);
    }
}
