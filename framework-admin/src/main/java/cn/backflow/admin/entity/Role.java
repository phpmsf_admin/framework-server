package cn.backflow.admin.entity;

import cn.backflow.data.entity.BaseEntity;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Role extends BaseEntity {

    @NotBlank(message = "角色名不能为空")
    @Size(max = 32)
    private String name;

    private Integer creator;

    @Size(max = 128)
    private String description;

    @Max(127)
    private Integer state = 1;

    public Role() {
    }

    public Role(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public Integer getState() {
        return this.state;
    }

    public void setState(Integer value) {
        this.state = value;
    }

    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }
}