<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!-- 不使用namespace的话sql搜索定位会比较方便 -->
<mapper namespace="Department">

  <resultMap id="UserDepartmentMap" type="cn.backflow.admin.entity.UserDepartment">
    <result property="id" column="id"/>
    <result property="userId" column="user_id"/>
    <result property="departmentId" column="department_id"/>
  </resultMap>

  <insert id="insertUserDepartmentBatch" useGeneratedKeys="true">
    INSERT INTO t_user_department (user_id, department_id) VALUES
    <foreach collection="list" item="item" separator=",">
      (#{item.userId}, #{item.departmentId})
    </foreach>
  </insert>

  <delete id="deleteUserDepartment">
    DELETE FROM t_user_department
    <include refid="UserDepartmentDynamicWhere"/>
  </delete>

  <select id="findUserDepartment" resultMap="UserDepartmentMap">
    SELECT id, user_id, department_id FROM t_user_department
    <include refid="UserDepartmentDynamicWhere"/>
  </select>

  <sql id="UserDepartmentDynamicWhere">
    <where>
      <if test="id neq null">AND id = #{id}</if>
      <if test="userId neq null">AND user_id = #{userId}</if>
      <if test="departmentId neq null">AND department_id = #{departmentId}</if>
    </where>
  </sql>

  <resultMap id="DepartmentMap" type="cn.backflow.admin.entity.Department">
    <result property="id" column="id"/>
    <result property="name" column="name"/>
    <result property="leader" column="leader"/>
    <result property="leaderName" column="leader_name"/>
    <result property="parent" column="parent"/>
    <result property="ancestors" column="ancestors"/>
    <result property="level" column="level"/>
    <result property="tel" column="tel"/>
    <result property="seq" column="seq"/>
    <result property="created" column="created"/>
    <result property="updated" column="updated"/>
  </resultMap>

  <!-- 用于select查询公用抽取的列 -->
  <sql id="Department.columns">
    id, name, leader, parent, ancestors, level, seq, created, updated
  </sql>

  <sql id="Department.fullcolumns">
    d.id, d.name, leader, u.name leader_name, parent, ancestors, level, seq, d.created, d.updated
  </sql>

  <!-- useGeneratedKeys="true" keyProperty="xxx" for sqlserver and mysql -->
  <insert id="insert" useGeneratedKeys="true" keyProperty="id">
    INSERT INTO t_department (
      name,
      leader,
      parent,
      ancestors,
      level,
      seq,
      created
    ) VALUES (
      #{name},
      #{leader},
      #{parent},
      #{ancestors},
      #{level},
      #{seq},
      now()
    )
  </insert>

  <update id="update">
    UPDATE t_department SET
      name = #{name},
      leader = #{leader},
      parent = #{parent},
      ancestors = #{ancestors},
      level = #{level},
      seq = #{seq},
      updated = now()
    WHERE
      id = #{id}
  </update>

  <update id="updateSelective">
    UPDATE t_department SET
    <if test="name neq null">name = #{name},</if>
    <if test="leader neq null">leader = #{leader},</if>
    <if test="parent neq null">parent = #{parent},</if>
    <if test="ancestors neq null">ancestors = #{ancestors},</if>
    <if test="level neq null">level = #{level},</if>
    <if test="seq neq null">seq = #{seq},</if>
    updated = now()
    WHERE
    id = #{id}
  </update>

  <delete id="delete">
    DELETE FROM t_department WHERE id = #{id}
  </delete>

  <delete id="deleteBatch">
    DELETE FROM t_department WHERE id IN
    <foreach collection="collection" item="id" open="(" close=")" separator=",">#{id}</foreach>
  </delete>

  <select id="getById" resultMap="DepartmentMap">
    SELECT
    <include refid="Department.columns"/>
    FROM t_department WHERE id = #{id}
  </select>

  <select id="findAll" resultMap="DepartmentMap">
    SELECT
    <include refid="Department.fullcolumns"/>
    FROM t_department d LEFT JOIN t_user u ON d.leader = u.id
    <include refid="DepartmentFullDynamicWhere"/>
    <choose>
      <when test="_sort_ neq null">
        ORDER BY ${_sort_}
      </when>
      <otherwise>
        ORDER BY seq
      </otherwise>
    </choose>
  </select>

  <select id="findSiblings" resultMap="DepartmentMap">
    SELECT
    <include refid="Department.columns"/>
    FROM t_department
    WHERE parent = (
    SELECT parent FROM t_department WHERE id = #{id}
    )
    <if test="excludeCurrent">
      AND id != #{id}
    </if>
    <choose>
      <when test="_sort_ neq null">
        ORDER BY ${_sort_}
      </when>
      <otherwise>
        ORDER BY seq
      </otherwise>
    </choose>
  </select>

  <sql id="DepartmentDynamicWhere">
    <where>
      <if test="name neq null">AND name LIKE CONCAT('%', #{name}, '%')</if>
      <if test="leader neq null">AND leader LIKE CONCAT('%', #{leader}, '%')</if>
      <if test="level neq null">AND level = #{level}</if>
      <if test="parent neq null">
        <choose>
          <when test="direct neq null and direct">AND parent = #{parent}</when>
          <otherwise>AND ancestors LIKE CONCAT((SELECT ancestors FROM t_permission WHERE id = #{parent}), '%')</otherwise>
        </choose>
      </if>
      <if test="createdBegin neq null">AND created >= #{createdBegin}</if>
      <if test="createdEnd neq null">AND created &lt;= #{createdEnd}</if>
      <if test="updatedBegin neq null">AND updated >= #{updatedBegin}</if>
      <if test="updatedEnd neq null">AND updated &lt;= #{updatedEnd}</if>
    </where>
  </sql>

  <sql id="DepartmentFullDynamicWhere">
    <where>
      <if test="name neq null">AND d.name LIKE CONCAT('%', #{name}, '%')</if>
      <if test="leader neq null">AND leader LIKE CONCAT('%', #{leader}, '%')</if>
      <if test="level neq null">AND level = #{level}</if>
      <if test="parent neq null">AND parent = #{parent} OR ancestors LIKE CONCAT('%', #{parent}, ',%')</if>
      <if test="createdBegin neq null">AND d.created >= #{createdBegin}</if>
      <if test="createdEnd neq null">AND d.created &lt;= #{createdEnd}</if>
      <if test="updatedBegin neq null">AND d.updated >= #{updatedBegin}</if>
      <if test="updatedEnd neq null">AND d.updated &lt;= #{updatedEnd}</if>
    </where>
  </sql>

  <select id="pagingCount" resultType="long">
    SELECT COUNT(id) FROM t_department
    <include refid="DepartmentDynamicWhere"/>
  </select>

  <select id="paging" resultMap="DepartmentMap">
    SELECT
    <include refid="Department.fullcolumns"/>
    FROM t_department d, t_user u
    <include refid="DepartmentFullDynamicWhere"/>

    <choose>
      <when test="_sort_ neq null">
        ORDER BY ${_sort_}
      </when>
      <otherwise>
        ORDER BY seq
      </otherwise>
    </choose>

    <if test="_offset_ neq null">
      LIMIT ${_offset_}, ${_limit_}
    </if>
  </select>

</mapper>