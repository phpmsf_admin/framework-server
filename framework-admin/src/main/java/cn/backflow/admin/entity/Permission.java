package cn.backflow.admin.entity;

import cn.backflow.data.entity.BaseEntity;
import cn.backflow.web.treeable.Treeable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class Permission extends BaseEntity implements Treeable {

    @Size(max = 64)
    @NotBlank(message = "权限编码不能为空")
    private String code;

    @Size(max = 64)
    @NotBlank(message = "权限名称不能为空")
    private String name;

    @Size(max = 255)
    private String description;

    private String icon;

    private Integer parent = 0;

    @Size(max = 255)
    private String ancestors;

    private Integer level = 1;

    private Integer seq;

    private List<Treeable> children;

    public void addChild(Treeable treeable) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(treeable);
    }

    @Override
    public List<Treeable> getChildren() {return children;}

    @Override
    public Treenode asTreenode() {
        Treenode node = new Treenode();
        node.id = this.id;
        node.pid = this.parent;
        node.name = this.name;
        node.icon = this.icon;
        return node;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String value) {
        this.code = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getParent() {
        return this.parent;
    }

    public void setParent(Integer value) {
        this.parent = value;
    }

    public String getAncestors() {
        return this.ancestors;
    }

    public void setAncestors(String value) {
        this.ancestors = value;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Integer getLevel() {
        return this.level;
    }

    public void setLevel(Integer value) {
        this.level = value;
    }
}