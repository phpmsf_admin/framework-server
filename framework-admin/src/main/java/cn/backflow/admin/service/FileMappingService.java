package cn.backflow.admin.service;

import cn.backflow.admin.entity.FileMapping;
import cn.backflow.admin.repository.FileMappingRepository;
import cn.backflow.data.pagination.Page;
import cn.backflow.data.pagination.PageRequest;
import cn.backflow.data.service.AbstractService;
import cn.backflow.utils.Qiniu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FileMappingService extends AbstractService<FileMapping, Integer> {

    private final FileMappingRepository fileMappingRepo;

    @Autowired
    public FileMappingService(FileMappingRepository fileMappingDao) {
        this.fileMappingRepo = fileMappingDao;
    }

    public int save(List<FileMapping> mappings) {
        if (mappings == null || mappings.isEmpty())
            return 0;
        return fileMappingRepo.save(mappings);
    }

    public List<FileMapping> findByIds(List<Integer> ids) {
        if (ids == null || ids.isEmpty()) return Collections.emptyList();
        return fileMappingRepo.findByIds(ids);
    }

    public List<FileMapping> findByKeys(List<String> keys) {
        if (keys == null || keys.isEmpty()) return Collections.emptyList();
        return fileMappingRepo.findByKeys(keys);
    }

    public Map<String, FileMapping> findMapByKeys(List<String> keys) {
        return fileMappingRepo.findMapByKeys(keys, "key");
    }

    public List<String> findKeyByIds(Collection<Integer> ids) {
        return fileMappingRepo.findKeyByIds(ids);
    }

    public Page<FileMapping> findByFolder(String folder, int pageNumber, int pageSize) {
        PageRequest pr = new PageRequest();
        pr.addFilter("folder", folder);
        pr.setSortColumns("id desc");
        pr.setPageNumber(pageNumber);
        pr.setPageSize(pageSize);
        return fileMappingRepo.findByPageRequest(pr);
    }

    /**
     * 查找所有父文件夹
     *
     * @param id 文件id
     * @return 父文件夹列表(包含当前文件)
     */
    public List<FileMapping> findParentsById(Integer id) {
        FileMapping current = getById(id);
        if (current.getParent() == 0 || current.getAncestors() == null) {
            return Collections.singletonList(current);
        }
        String[] arr = current.getAncestors().split(",");
        List<Integer> ids = Stream.of(arr)
                .mapToInt(Integer::parseInt)
                .boxed()
                .collect(Collectors.toList());

        ids.add(id);
        return fileMappingRepo.findByIds(ids);
    }

    @Override
    @Transactional
    public int saveOrUpdate(FileMapping mapping) throws DataAccessException {
        return mapping.getId() == null ? save(mapping) : update(mapping);
    }

    @Override
    @Transactional
    public int save(FileMapping mapping) throws DataAccessException {
        setAncestors(mapping);
        return super.save(mapping);
    }

    @Override
    @Transactional
    public int update(FileMapping mapping) throws DataAccessException {
        setAncestors(mapping);
        return super.updateSelective(mapping);
    }

    /* 设置文件祖先ID路径 */
    private void setAncestors(FileMapping mapping) {
        Integer parent = mapping.getParent();
        if (parent != 0) {
            FileMapping p = getById(parent);
            String ancestors = p.getAncestors();
            if (ancestors == null) {
                ancestors = p.getId().toString();
            } else {
                ancestors = ancestors + "," + p.getId();
            }
            mapping.setAncestors(ancestors);
        }
    }

    @Transactional
    public int deleteByKeys(String[] keys) {
        Qiniu.delete(keys);
        return fileMappingRepo.deleteByKeys(keys);
    }

    @Override
    public int deleteById(Integer id) throws DataAccessException {
        return super.deleteById(id);
    }

    @Override
    @Transactional
    public int deleteBatch(Collection<Integer> ids) {
        if (ids.isEmpty()) return 0;
        List<String> keys = findKeyByIds(ids);
        Qiniu.delete(keys.toArray(new String[0]));
        return super.deleteBatch(ids);
    }

}