package cn.backflow.admin.service;

import cn.backflow.admin.Constants;
import cn.backflow.admin.entity.Dict;
import cn.backflow.admin.entity.Permission;
import cn.backflow.admin.entity.User;
import cn.backflow.admin.repository.DictRepository;
import cn.backflow.admin.repository.PermissionRepository;
import cn.backflow.admin.repository.RoleRepository;
import cn.backflow.data.service.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@CacheConfig(cacheNames = Constants.PERMISSION_CACHE)
public class PermissionService extends AbstractService<Permission, Integer> {

    private final PermissionRepository permissionRepo;
    private final RoleRepository roleRepo;
    private final DictRepository dictRepo;

    @Autowired
    public PermissionService(PermissionRepository permissionRepo, RoleRepository roleRepo, DictRepository dictRepo) {
        this.permissionRepo = permissionRepo;
        this.roleRepo = roleRepo;
        this.dictRepo = dictRepo;
    }

    public List<Permission> findAll() throws DataAccessException {
        return super.findAll(null);
    }

    public Map<Comparable, Permission> findMap() throws DataAccessException {
        return permissionRepo.findMap(null, "id");
    }

    /**
     * 查询当前用户的所有权限
     *
     * @param user 当前登录用户
     * @return Map&lt;PK, Permission&gt;
     */
    public Map<Comparable, Permission> findMap(User user) {
        Map<String, Object> params = null;
        if (user != null && !user.isAdmin()) {
            params = Collections.singletonMap("userId", user.getId());
        }
        return permissionRepo.findMap(params, "id");
    }

    /**
     * 查找子元素
     *
     * @param parent 父权限ID
     * @param direct 是否只查找直接子元素
     * @return 该权限的直接子权限（<code>direct</code>为<code>true</code>是返回不限层级的所有子权限）
     */
    public List<Permission> findByParent(Integer parent, boolean direct) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("parent", parent);
        parameter.put("direct", direct);
        return permissionRepo.findAll(parameter);
    }

    /**
     * 更新权限排序
     *
     * @param id   权限ID
     * @param from 起始位置
     * @param to   结束位置
     * @return 影响记录数
     */
    @Transactional
    public int updateSeq(Integer id, Integer from, Integer to) {
        List<Permission> siblings = permissionRepo.findSiblings(id, false);
        for (int i = 0, len = siblings.size(); i < len; i++) {
            Permission d = siblings.get(i);
            d.setSeq(i);
            if (i == from) {
                d.setSeq(to);
            }
            if (i == to) {
                d.setSeq(from);
            }
        }
        // TODO 优化: 通过UPDATE单个seq字段更新
        return permissionRepo.updateBatch(siblings);
    }

    @Override
    @Transactional
    public int save(Permission entity) throws DataAccessException {
        setPermissionAncestorAndLevel(entity, null);
        return super.saveOrUpdate(entity);
    }


    @Transactional
    public int update(Permission entity) throws DataAccessException {
        setPermissionAncestorAndLevel(entity, null);
        int rows = super.saveOrUpdate(entity);
        permissionRepo.updateChildrenAncestors(entity);
        return rows;
    }

    @Override
    @Transactional
    public int deleteById(Integer id) throws DataAccessException {
        List<Permission> children = findByParent(id, true);
        if (children.size() > 0)
            throw new IncorrectResultSizeDataAccessException("您要删除的记录含有子元素, 删除失败!", 1);
        roleRepo.deleteRolePermissionsByPermissionId(id);
        return super.deleteById(id);
    }

    /**
     * 与子权限一并保存
     *
     * @param parent   权限对象
     * @param children 子权限标识 (通过数据字典`default_permission_group`获取明细)
     */
    @Transactional
    public int saveWithChildren(Permission parent, String[] children) {
        int effected = save(parent);
        if (children == null || children.length == 0) {
            return effected;
        }
        // 从字典获取默认权限组
        Map<Comparable, Dict> dicts = dictRepo.findMapByCode("default_permission_group", "key");

        // 保存相应的权限
        for (String child : children) {
            Dict d = dicts.get(child);
            Permission p = new Permission();
            p.setName(d.getValue() + parent.getName());
            p.setCode(parent.getCode() + "." + d.getKey());
            p.setIcon(d.getComment());
            p.setSeq(d.getSeq());
            p.setParent(parent.getId());
            d.setDescription(p.getName());
            setPermissionAncestorAndLevel(p, parent);
            update(p);
        }
        return effected;
    }

    /**
     * 设置权限层级与祖先路径
     */
    @Transactional
    public void setPermissionAncestorAndLevel(Permission permission, Permission parent) {
        if (permission.getId() == null)
            permissionRepo.insert(permission);
        int level = 1;
        String ancestors = permission.getId().toString();
        if (permission.getParent() != null && permission.getParent() != 0) {
            if (parent == null || !Objects.equals(parent.getId(), permission.getParent())) {
                parent = getById(permission.getParent());
            }
            level = parent.getLevel() + 1;
            ancestors = parent.getAncestors() + "," + ancestors;
        }
        permission.setLevel(level);
        permission.setAncestors(ancestors);
    }
}