package cn.backflow.scheduling;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@TestConfiguration
public class TestScheduleCustomizer {
}
